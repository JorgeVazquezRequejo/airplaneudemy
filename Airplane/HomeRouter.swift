//
//  HomeRouter.swift
//  Airplane
//
//  Created by Jorge Vazquez on 18/03/2020.
//  Copyright © 2020 Jorge Vazquez. All rights reserved.
//

import UIKit

protocol HomeRouting {
    
}

class HomeRouter {
    
    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
}

extension HomeRouter: HomeRouting {
    
}
