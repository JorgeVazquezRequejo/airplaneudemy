//
//  ViewController.swift
//  Airplane
//
//  Created by Jorge Vazquez on 18/03/2020.
//  Copyright © 2020 Jorge Vazquez. All rights reserved.
//

import UIKit

protocol HomeView: class{
    
}

class HomeViewController: UIViewController {

    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

extension HomeViewController: HomeView{
    
}
