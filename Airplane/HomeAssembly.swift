//
//  HomeAssembly.swift
//  Airplane
//
//  Created by Jorge Vazquez on 18/03/2020.
//  Copyright © 2020 Jorge Vazquez. All rights reserved.
//

import UIKit

class HomeAssembly {
    
    static func build() -> UIViewController{
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let interactor = HomeInteractor()
        let router = HomeRouter(view: view)
        let presenter = HomePresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        
        view.view.backgroundColor = .black
        return view
    }
}
